package com.breezzo.notifications.service;

import com.breezzo.notifications.domain.EventType;
import com.breezzo.notifications.domain.NotificationType;
import com.breezzo.notifications.domain.Subscriber;
import com.breezzo.notifications.repository.SubscriberRepository;
import com.breezzo.notifications.resource.dto.SubscribeRequest;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.Set;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@Service
public class SubscriberService {

    @Autowired
    private SubscriberRepository subscriberRepository;

    public Mono<Subscriber> subscribe(SubscribeRequest request) {
        return subscriberRepository.findByUserId(request.getUserId())
                .map(subscriber -> {
                    Set<EventType> eventTypes = Sets.union(subscriber.getEventTypes(), request.getEventTypes());
                    Set<NotificationType> notificationTypes = Sets.union(subscriber.getNotificationTypes(), request.getNotificationTypes());
                    subscriber.setEventTypes(eventTypes);
                    subscriber.setNotificationTypes(notificationTypes);
                    return subscriber;
                })
                .switchIfEmpty(Mono.defer(() -> {
                    Subscriber subscriber = new Subscriber();
                    subscriber.setUserId(request.getUserId());
                    subscriber.setEventTypes(request.getEventTypes());
                    subscriber.setNotificationTypes(request.getNotificationTypes());
                    return Mono.just(subscriber);
                }))
                .flatMap(subscriberRepository::save);
    }
}
