package com.breezzo.notifications.service;

import com.breezzo.notifications.domain.Notification;
import com.breezzo.notifications.repository.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@Service
public class NotificationService {

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    @Autowired
    private NotificationRepository notificationRepository;

    public Flux<Notification> getNewNotifications(Long userId) {
        return notificationRepository.getByReceivedFalseAndUserId(userId);
    }

    public void receiveNotification(String notificationId) {
        notificationRepository.findById(notificationId)
                .flatMap(notification -> {
                    notification.setReceived(true);
                    return notificationRepository.save(notification);
                }).subscribe(notification -> {
                    logger.debug("Notification {} received", notificationId);
                });
    }
}
