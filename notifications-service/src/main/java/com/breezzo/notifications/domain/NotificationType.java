package com.breezzo.notifications.domain;

/**
 * @author breezzo
 * @since 1/28/18.
 */
public enum NotificationType {
    SMS, EMAIL, SITE, ALL
}
