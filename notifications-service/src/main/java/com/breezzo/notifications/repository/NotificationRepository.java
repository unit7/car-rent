package com.breezzo.notifications.repository;

import com.breezzo.notifications.domain.Notification;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@Repository
public interface NotificationRepository extends ReactiveCrudRepository<Notification, String> {

    Flux<Notification> getByReceivedFalseAndUserId(Long userId);
}
