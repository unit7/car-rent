package com.breezzo.notifications.repository;

import com.breezzo.notifications.domain.Subscriber;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@Repository
public interface SubscriberRepository extends ReactiveCrudRepository<Subscriber, String> {

    Mono<Subscriber> findByUserId(Long userId);
}
