package com.breezzo.notifications.resource;

import com.breezzo.notifications.domain.Subscriber;
import com.breezzo.notifications.resource.dto.SubscribeRequest;
import com.breezzo.notifications.service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@RequestMapping("/notifications/subscribe")
@RestController
public class SubscribingResource {

    @Autowired
    private SubscriberService subscriberService;

    /**
     * @return updated subscriber
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Subscriber> subscribe(@RequestBody @Valid SubscribeRequest request) {
        return subscriberService.subscribe(request);
    }
}
