package com.breezzo.notifications.resource.dto;

import com.breezzo.notifications.domain.EventType;
import com.breezzo.notifications.domain.NotificationType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * @author breezzo
 * @since 1/28/18.
 */
public class SubscribeRequest {
    @NotNull
    private Long userId;

    @NotNull
    @Size(min = 1)
    private Set<EventType> eventTypes;

    @NotNull
    @Size(min = 1)
    private Set<NotificationType> notificationTypes;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(Set<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public Set<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(Set<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }
}
