package com.breezzo.notifications.resource;

import com.breezzo.notifications.domain.Notification;
import com.breezzo.notifications.resource.dto.NotificationView;
import com.breezzo.notifications.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@RequestMapping("/notifications")
@RestController
public class NotificationResource {

    @Autowired
    private NotificationService notificationService;

    @GetMapping("/{userId}")
    public Flux<NotificationView> getNewNotifications(@PathVariable("userId") Long userId) {
        return notificationService.getNewNotifications(userId).map(this::toView);
    }

    private NotificationView toView(Notification notification) {
        NotificationView view = new NotificationView();
        view.setEventType(notification.getEventType());
        view.setTitle(notification.getTitle());
        view.setMessageBody(notification.getMessageBody());
        view.setNotificationId(notification.getId());
        return view;
    }

    @PutMapping("/received/{notificationId}")
    public void notificationReceived(@PathVariable("notificationId") String notificationId) {
        notificationService.receiveNotification(notificationId);
    }
}
