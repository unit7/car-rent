package com.breezzo.notifications.resource.dto;

import com.breezzo.notifications.domain.EventType;

/**
 * @author breezzo
 * @since 1/28/18.
 */
public class NotificationView {
    private String notificationId;
    private EventType eventType;
    private String title;
    private String messageBody;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
