package com.breezzo.notifications;

import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringCloudApplication
@EnableReactiveMongoRepositories("com.breezzo.notifications.repository")
@ComponentScan
@EnableConfigurationProperties(MongoProperties.class)
public class NotificationsServiceApplication extends AbstractReactiveMongoConfiguration {

	@Value("${application.database}")
	private String databaseName;

	@Autowired
	private MongoProperties mongoProperties;

	public static void main(String[] args) {
		SpringApplication.run(NotificationsServiceApplication.class, args);
	}

	@Override
	protected String getDatabaseName() {
		return databaseName;
	}

	@Override
	public com.mongodb.reactivestreams.client.MongoClient reactiveMongoClient() {
		return MongoClients.create(mongoProperties.getUri());
	}
}
