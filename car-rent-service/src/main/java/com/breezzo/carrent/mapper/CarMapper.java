package com.breezzo.carrent.mapper;

import com.breezzo.carrent.domain.Car;
import com.breezzo.carrent.web.dto.CarView;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CarMapper {

    @Mapping(source = "vendor.name", target = "vendorName")
    CarView toView(Car car);
}
