package com.breezzo.carrent.service;

import com.breezzo.carrent.client.NotificationClient;
import com.breezzo.carrent.client.SubscribeRequest;
import com.breezzo.carrent.domain.Car;
import com.breezzo.carrent.domain.CarRequest;
import com.breezzo.carrent.domain.CarStock;
import com.breezzo.carrent.repository.CarRepository;
import com.breezzo.carrent.repository.CarRequestRepository;
import com.breezzo.carrent.repository.CarStockRepository;
import com.breezzo.carrent.web.dto.CarRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@Service
public class CarRequestService {

    @Autowired
    private CarRequestRepository carRequestRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarStockRepository carStockRepository;

    @Autowired
    private NotificationClient notificationClient;

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void createCarRequest(CarRequestDto carRequestDto) {
        Car car = carRepository.findById(carRequestDto.getCarId())
                .orElseThrow(() -> new IllegalArgumentException("No car found with id=" + carRequestDto.getCarId()));

        CarRequest carRequest = new CarRequest();
        carRequest.setUserId(carRequestDto.getUserId());
        carRequest.setRequestedCar(car);

        carRequestRepository.save(carRequest);

        CarStock carStock = carStockRepository.findByCar(car);
        boolean isCarAvailable = carStock.getAvailableCount() > 0;

        if (isCarAvailable) {
            carStock.addBookedCount(1);
            carStockRepository.save(carStock);
        }

        if (carRequestDto.isNotifyWhenInStock() && !isCarAvailable) {
            notificationClient.subscribe(SubscribeRequest.of(carRequest.getUserId(), SubscribeRequest.EventType.CAR_IN_STOCK));
        }
    }
}
