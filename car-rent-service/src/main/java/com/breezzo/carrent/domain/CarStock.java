package com.breezzo.carrent.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@Entity
@Table(name = "car_stock")
public class CarStock extends AbstractEntity {
    @OneToOne(optional = false)
    private Car car;

    @NotNull
    @Column(name = "count")
    private Integer count;

    @NotNull
    @Column(name = "booked_count")
    private Integer bookedCount;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getBookedCount() {
        return bookedCount;
    }

    public void setBookedCount(Integer bookedCount) {
        this.bookedCount = bookedCount;
    }

    public void addBookedCount(int count) {
        bookedCount += count;
    }

    public int getAvailableCount() {
        return count - bookedCount;
    }
}
