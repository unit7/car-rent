package com.breezzo.carrent.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author breezzo
 * @since 1/20/18.
 */
@Entity
@Table(name = "vendor")
public class Vendor extends AbstractEntity {
    @NotNull
    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "name='" + name + '\'' +
                "} " + super.toString();
    }
}
