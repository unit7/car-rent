package com.breezzo.carrent.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@Entity
@Table(name = "car_request")
public class CarRequest extends AbstractEntity {
    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "car_id", nullable = false)
    private Car requestedCar;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Car getRequestedCar() {
        return requestedCar;
    }

    public void setRequestedCar(Car requestedCar) {
        this.requestedCar = requestedCar;
    }
}
