package com.breezzo.carrent.repository.specification;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public enum QueryOperator {
    LT, GT, EQ;

    public static QueryOperator fromString(String rawOperator) {
        switch (rawOperator) {
            case "<":
                return LT;
            case ">":
                return GT;
            case "=":
                return EQ;
        }
        throw new IllegalArgumentException("Operator '" + rawOperator + "' is not supported");
    }
}
