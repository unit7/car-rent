package com.breezzo.carrent.repository;

import com.breezzo.carrent.domain.Car;
import com.breezzo.carrent.domain.CarStock;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public interface CarStockRepository extends JpaRepository<CarStock, Long> {

    CarStock findByCar(Car car);
}
