package com.breezzo.carrent.repository.specification;

import com.breezzo.carrent.domain.Car;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public class CarSpecification implements Specification<Car> {
    private final QueryCondition condition;

    public CarSpecification(QueryCondition condition) {
        this.condition = condition;
    }

    @Override
    public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        switch (condition.getOperator()) {
            case LT:
                return cb.lessThan(mapField(condition.getField(), root), condition.getValue());
            case GT:
                return cb.greaterThan(mapField(condition.getField(), root), condition.getValue());
            case EQ:
                return mapEqOperator(mapField(condition.getField(), root), condition.getValue(), cb);
        }
        return cb.disjunction();
    }

    private static <T> Predicate mapEqOperator(Path<T> field, String value, CriteriaBuilder cb) {
        if (field.getJavaType() == String.class) {
            return cb.like(cb.lower((Path<String>) field), "%" + StringUtils.lowerCase(value) + "%");
        } else {
            return cb.equal(field, value);
        }
    }

    private static <T> Path<T> mapField(String fieldName, Root<Car> root) {
        switch (fieldName) {
            case "vendorName":
                return root.get("vendor").get("name");
            default:
                return root.get(fieldName);
        }
    }
}
