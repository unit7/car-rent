package com.breezzo.carrent.repository;

import com.breezzo.carrent.domain.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author breezzo
 * @since 1/21/18.
 */
@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {
}
