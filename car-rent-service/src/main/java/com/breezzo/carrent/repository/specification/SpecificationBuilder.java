package com.breezzo.carrent.repository.specification;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public class SpecificationBuilder {
    private final List<QueryCondition> conditions = new ArrayList<>();

    public static SpecificationBuilder create() {
        return new SpecificationBuilder();
    }

    public SpecificationBuilder with(String field, String operator, String value) {
        conditions.add(QueryCondition.of(field, operator, value));
        return this;
    }

    public CarSpecification build() {
        Preconditions.checkState(!conditions.isEmpty(), "should exists at least one condition");
        CarSpecification spec = new CarSpecification(conditions.get(0));
        return conditions.stream()
                .skip(1)
                .map(CarSpecification::new)
                .reduce(spec, (a, b) -> (CarSpecification) a.and(b));
    }
}
