package com.breezzo.carrent.repository.specification;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public class QueryCondition {
    private final String field;
    private final QueryOperator operator;
    private final String value;

    public QueryCondition(String field, QueryOperator operator, String value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    public static QueryCondition of(String field, String operator, String value) {
        return new QueryCondition(field, QueryOperator.fromString(operator), value);
    }

    public String getField() {
        return field;
    }

    public QueryOperator getOperator() {
        return operator;
    }

    public String getValue() {
        return value;
    }
}
