package com.breezzo.carrent.repository;

import com.breezzo.carrent.domain.CarRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public interface CarRequestRepository extends JpaRepository<CarRequest, Long> {
}
