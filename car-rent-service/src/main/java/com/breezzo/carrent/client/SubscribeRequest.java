package com.breezzo.carrent.client;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author breezzo
 * @since 1/28/18.
 */
public class SubscribeRequest {
    private Long userId;
    private Set<EventType> eventTypes;
    private Set<NotificationType> notificationTypes;

    public static SubscribeRequest of(Long userId, EventType eventType, NotificationType... notificationTypes) {
        return of(userId, Sets.immutableEnumSet(eventType), notificationTypes);
    }

    public static SubscribeRequest of(Long userId, Set<EventType> eventTypes, NotificationType... notificationTypes) {
        List<NotificationType> nTypes = Arrays.asList(notificationTypes);
        if (nTypes.isEmpty()) {
            nTypes = Collections.singletonList(NotificationType.ALL);
        }

        SubscribeRequest request = new SubscribeRequest();
        request.setUserId(userId);
        request.setEventTypes(eventTypes);
        request.setNotificationTypes(Sets.immutableEnumSet(nTypes));
        return request;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(Set<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public Set<NotificationType> getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(Set<NotificationType> notificationTypes) {
        this.notificationTypes = notificationTypes;
    }

    public enum EventType {
        CAR_IN_STOCK
    }

    public enum NotificationType {
        SMS, EMAIL, SITE, ALL
    }
}
