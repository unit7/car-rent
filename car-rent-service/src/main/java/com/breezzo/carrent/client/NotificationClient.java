package com.breezzo.carrent.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author breezzo
 * @since 1/28/18.
 */
@FeignClient(name = "notifications-service", primary = false)
public interface NotificationClient {

    @PostMapping("/notifications/subscribe")
    Object subscribe(SubscribeRequest request);
}
