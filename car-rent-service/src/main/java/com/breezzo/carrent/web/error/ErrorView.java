package com.breezzo.carrent.web.error;

import java.util.ArrayList;
import java.util.List;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public class ErrorView {
    private final String message;
    private final List<FieldError> fieldErrors;

    public ErrorView(String message, List<FieldError> errors) {
        this.message = message;
        this.fieldErrors = errors;
    }

    public String getMessage() {
        return message;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder message(String message) {
        return new Builder().message(message);
    }

    public static class FieldError {
        private final String field;
        private final String message;

        public FieldError(String field, String message) {
            this.field = field;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class Builder {
        private String message;
        private List<FieldError> errors = new ArrayList<>();

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder withError(String field, String message) {
            errors.add(new FieldError(field, message));
            return this;
        }

        public ErrorView build() {
            return new ErrorView(message, errors);
        }
    }
}
