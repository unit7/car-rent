package com.breezzo.carrent.web;

import com.breezzo.carrent.domain.Car;
import com.breezzo.carrent.mapper.CarMapper;
import com.breezzo.carrent.repository.CarRepository;
import com.breezzo.carrent.repository.specification.SpecificationBuilder;
import com.breezzo.carrent.web.dto.CarView;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@RequestMapping("/car")
@RestController
public class CarPagingResource {

    @Autowired
    private CarMapper carMapper;

    @Autowired
    private CarRepository carRepository;

    @GetMapping("/page")
    public Page<CarView> getPage(@RequestParam("query") String query,
                                 @RequestParam("page") int page,
                                 @RequestParam("pageSize") int pageSize,
                                 @RequestParam(value = "sort", required = false) String sortField,
                                 @RequestParam(value = "sortOrder", required = false) String sortOrder) {

        Specification<Car> specification = buildSpecification(query);

        Sort.Direction sortDirection = Sort.Direction.fromOptionalString(sortOrder).orElse(Sort.Direction.ASC);
        Sort sort = Strings.isNullOrEmpty(sortField) ? Sort.unsorted() : Sort.by(sortDirection, sortField);
        PageRequest pageRequest = PageRequest.of(page, pageSize, sort);

        return carRepository.findAll(specification, pageRequest).map(carMapper::toView);

    }

    private Specification<Car> buildSpecification(String query) {
        if (Strings.isNullOrEmpty(query)) {
            return null;
        }

        String[] fieldConditions = query.split(";");
        SpecificationBuilder specificationBuilder = SpecificationBuilder.create();
        for (String fieldCondition : fieldConditions) {
            String[] conditions = fieldCondition.split(":");
            specificationBuilder.with(conditions[0], conditions[1], conditions[2]);
        }

        return specificationBuilder.build();
    }
}
