package com.breezzo.carrent.web;

import com.breezzo.carrent.domain.Vendor;
import com.breezzo.carrent.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author breezzo
 * @since 1/21/18.
 */
@RestController
@RequestMapping("/vendor")
public class VendorResource {

    @Autowired
    private VendorRepository vendorRepository;

    @PostMapping
    public Vendor createVendor(@RequestBody Vendor vendor) {
        return vendorRepository.save(vendor);
    }
}
