package com.breezzo.carrent.web;

import com.breezzo.carrent.domain.Car;
import com.breezzo.carrent.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author breezzo
 * @since 1/21/18.
 */
@RestController
@RequestMapping("/car")
public class CarResource {

    @Autowired
    private CarRepository carRepository;

    @GetMapping
    public List<Car> findAll() {
        return carRepository.findAll();
    }

    @GetMapping("/{id}")
    public Mono<Car> findOne(@PathVariable("id") Long id) {
        return carRepository.findById(id).map(Mono::just).orElse(Mono.empty());
    }

    @PostMapping
    public Car create(@RequestBody Car car) {
        return carRepository.save(car);
    }
}

