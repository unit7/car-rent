package com.breezzo.carrent.web;

import com.breezzo.carrent.service.CarRequestService;
import com.breezzo.carrent.web.dto.CarRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@RequestMapping("/car/request")
@RestController
public class CarRequestResource {

    @Autowired
    private CarRequestService carRequestService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void requestCar(@Valid @RequestBody CarRequestDto carRequest) {
        carRequestService.createCarRequest(carRequest);
    }
}
