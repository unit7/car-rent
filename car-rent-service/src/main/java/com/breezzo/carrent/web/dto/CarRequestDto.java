package com.breezzo.carrent.web.dto;

import javax.validation.constraints.NotNull;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public class CarRequestDto {
    @NotNull
    private Long carId;

    @NotNull
    private Long userId;

    private boolean notifyWhenInStock;

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public boolean isNotifyWhenInStock() {
        return notifyWhenInStock;
    }

    public void setNotifyWhenInStock(boolean notifyWhenInStock) {
        this.notifyWhenInStock = notifyWhenInStock;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
