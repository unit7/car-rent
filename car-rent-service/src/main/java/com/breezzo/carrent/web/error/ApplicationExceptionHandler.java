package com.breezzo.carrent.web.error;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * @author breezzo
 * @since 1/27/18.
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorView processMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        ErrorView.Builder errorBuilder = ErrorView.builder();

        List<FieldError> fieldErrors = result.getFieldErrors();
        fieldErrors.forEach(fe -> errorBuilder.withError(fe.getField(), fe.getDefaultMessage()));

        List<ObjectError> globalErrors = result.getGlobalErrors();
        globalErrors.forEach(oe -> errorBuilder.withError(oe.getObjectName(), oe.getDefaultMessage()));

        return errorBuilder.build();
    }
}
