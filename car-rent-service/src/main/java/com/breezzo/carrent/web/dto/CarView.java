package com.breezzo.carrent.web.dto;

/**
 * @author breezzo
 * @since 1/27/18.
 */
public class CarView {
    private Long id;
    private String name;
    private String vendorName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
}
